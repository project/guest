<?php

/**
 * @file
 * Part of the "Guest" module that will be included for registering pathes.
 */

/**
 * Implementation of hook_menu().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_menu/6
 */
function guest_menu() {
  $items = array();
  // Item 1:
  $items['admin/guest/readme'] = array(
    'file' => 'includes/guest.menu.inc',
    'page callback' => 'guest_readme',
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
    'title' => GUEST_MODULE_FULLNAME . ': \'README.txt\'',
    'title callback' => FALSE,
  );
  return $items;
}

/**
 * Page callback function for Item 1 of function above.
 */
function guest_readme() {
  if (GUEST_DEBUG === TRUE) {
    menu_router_build();
    $menu_items = _guest_get_menu_items();
    ksort($menu_items);
    print '<pre>';
    foreach ($menu_items as $key => $value) {
      if (preg_match("/^user/", $key)) {
        // Only print keys starting with "user"
        print '[' . $key . '] => ';
        print_r($value);
        print "\n";
      }
    }
//  print_r($menu_items);
    print '</pre>';
  }

  $link_help = l(
    GUEST_MODULE_FULLNAME,
    'admin/help/guest',
    array(
      'attributes' => array(
        'title' => t('Help for') . ' ' . GUEST_MODULE_FULLNAME . ' ' . t('module'),
      ),
    )
  );

  $placeholders = array(
    '!link_help' => $link_help,
  );

  return "\n" . '<p><ul><li>' . t('Help for the !link_help module', $placeholders) . '</li></ul></p>';
}
