<?php

/**
 * @file
 * Part of the "Guest" module that will be included for displaying help.
 */

/**
 * Implementation of hook_help().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_help/6
 */
function guest_help($path, $arg) {
  switch ($path) {

    case 'admin/help#guest':
      $link_module = l(
        GUEST_MODULE_FULLNAME,
        GUEST_MODULE_HOMEPAGE,
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Homepage of') . ' ' . GUEST_MODULE_FULLNAME . ' ' . t('module'),
          ),
        )
      );
      $link_author = l(
        GUEST_AUTHOR_NAME,
        GUEST_AUTHOR_HOMEPAGE,
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Author of') . ' ' . GUEST_MODULE_FULLNAME . ' ' . t('module'),
          ),
        )
      );
      $link_roles = l(
        t('create a role'),
        'admin/user/roles',
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Admin page to create a role'),
          ),
        )
      );
      $link_user = l(
        t('create a user'),
        'admin/user/user/create',
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Admin page to create a user'),
          ),
        )
      );

      $title_link_permissions = t('Permission') . ' ' . GUEST_PERMISSION_EDIT;
      $link_permissions = l(
        GUEST_PERMISSION_EDIT,
        'admin/user/permissions',
        array(
          'fragment' => 'module-guest',
          'attributes' => array(
            'target' => '_blank',
            'title' => $title_link_permissions,
          ),
        )
      );
      $permissions_img = theme_image(
        GUEST_MY_URL . 'img/permission_edit_own_user_profile.png',
        $title_link_permissions,
        $title_link_permissions
      );
      $link_permissions_img = l(
        $permissions_img,
        'admin/user/permissions',
        array(
          'html' => TRUE,
          'fragment' => 'module-guest',
          'attributes' => array(
            'target' => '_blank',
          ),
        )
      );
      $link_readme = l(
        'README.txt',
        'admin/guest/readme',
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => 'README.txt ' . t('of') . ' ' . GUEST_MODULE_FULLNAME . ' ' . t('module'),
          ),
        )
      );

      $placeholders = array(
        '!link_module' => $link_module,
        '!link_author' => $link_author,
        '!link_roles' => $link_roles,
        '!link_user' => $link_user,
        '!link_permissions' => $link_permissions,
        '!link_permissions_img' => $link_permissions_img,
        '!link_readme' => $link_readme,
      );

      return "\n" . t(
        '<p>Welcome to the Help page of Drupal\'s !link_module module, developed by !link_author.</p>'
          . '<h3>Example of a use case</h3><ol>'
          . '<li>You !link_roles called <strong>guest</strong>.</li>'
          . '<li>You !link_user called <strong>Guest user</strong> that only belongs to the role created in step 1 above.</li>'
          . '<li>You set the permission "!link_permissions" to all roles except "anonymous user", "authenticated user" '
          . 'and the role created in step 1 above:<br />!link_permissions_img</li></ol>'
          . '<p>Complete documentation is available in the module\'s !link_readme file.</p>',
        $placeholders
      );
      break;

    case 'admin/guest/readme':
      $path_readme = GUEST_MY_PATH . 'README.txt';
      if (is_readable($path_readme)) {
        $contents_readme = file_get_contents($path_readme);
        if (!drupal_strlen($contents_readme)) {
          $contents_readme = t('Error: File could not be read!');
        }
      }
      else {
        $contents_readme = t('Error: File is not readable!');
      }
      return "\n" . '<p><pre style="padding: 10px; border: 1px solid;">' . check_plain($contents_readme) . '</pre></p>';
      break;
  }
}
