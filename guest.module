<?php

/**
 * @file
 * Main file of the "Guest" module.
 */

/**
 * Defining global constants
 */
//define('GUEST_DEBUG', TRUE);
  define('GUEST_DEBUG', FALSE);
  define('GUEST_MODULE_FULLNAME', 'Guest');
  define('GUEST_MODULE_HOMEPAGE', 'https://www.drupal.org/project/guest');
  define('GUEST_AUTHOR_NAME', 'Robert Allerstorfer');
  define('GUEST_AUTHOR_HOMEPAGE', 'https://www.drupal.org/user/405360');
  define('GUEST_PERMISSION_EDIT', 'edit own user profile');
  define('GUEST_MY_PATH', dirname(__FILE__) . '/');
  define('GUEST_MY_URL', drupal_get_path('module', 'guest') . '/');

module_load_include('inc', 'guest', 'includes/guest.help');
module_load_include('inc', 'guest', 'includes/guest.menu');

/**
 * Implementation of hook_perm().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_perm/6
 */
function guest_perm() {
  return array(GUEST_PERMISSION_EDIT);
}

/**
 * Implementation of hook_menu_alter().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_menu_alter/6
 */
function guest_menu_alter(&$items) {
  array_walk($items, '_guest_disable_user_edit_menu_items');
  if (GUEST_DEBUG === TRUE) {
    _guest_get_menu_items($items);
  }
}

/**
 * Helper function to disable all user edit menu items.
 * Called by array_walk() in function above.
 */
function _guest_disable_user_edit_menu_items(&$value, $key) {
  if (preg_match("/^user\/%user_category\/edit/", $key)) {
    // Every key starting with "user/%user_category/edit"
    if ($value['access callback'] && $value['access callback'] == 'user_edit_access') {
      $value['access callback'] = 'guest_edit_access';
    }
  }
}

/**
 * Access callback for user account editing.
 * Extended version of user.module's user_edit_access() function
 */
function guest_edit_access($account) {
  global $user;
  $uid = $user->uid;
  if ($uid && arg(1) == $uid && !user_access(GUEST_PERMISSION_EDIT)) {
    return FALSE;
  }
  return (($uid == $account->uid) || user_access('administer users')) && $account->uid > 0;
}

/**
 * Helper function for developers to get the array of all menu items.
 * Called only when GUEST_DEBUG is TRUE.
 */
function _guest_get_menu_items($items = array()) {
  static $menu_items = array();
  if (!empty($items)) {
    $menu_items = $items;
  }
  return $menu_items;
}

/**
 * Implementation of hook_user().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_user/6
 */
function guest_user($op, &$edit, &$account, $category = NULL) {
  global $user;
  $uid = $user->uid;
  if ($uid && arg(1) == $uid && !user_access(GUEST_PERMISSION_EDIT)) {
    if (drupal_strlen(arg(2))) {
      // When the user tries to call any path below "user/[UID]" (eg. "user/[UID]/edit" or "user/[UID]/edit/password")
      drupal_goto('user/' . $uid);
    }
  }
}
