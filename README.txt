################################################################################
#                                                                              #
#          Guest                                                               #
#          Version 6.x-1.2 (2015-04-17)                                        #
#                                                                              #
#          A module for Drupal 6.x                                             #
#                                                                              #
################################################################################

Web page
========

The project homepage is
  https://www.drupal.org/project/guest

Author
======

- Robert Allerstorfer (roball)
    https://www.drupal.org/user/405360

Overview
========

This module is useful for sites that share a guest account by more than one
person. For example, if you have a conference website and want to provide a
special download area to conference participants, you may consider creating a
role "guest" and one user "Guest user" that only belongs to that role. At the
conference you provide the username and password of that Guest user to all
participants.

Like the Anonymous user, the Guest user should not be able to edit anything on
the site, but just have additional view (download) access. Drupal core lacks to
support this situation. It only offers you disallowing the Guest to edit her
username, via the "change own username" permission, but may still allow the
Guest to edit other profile items such like password, e-mail address, upload
photo, custom profile fields and other settings. This module closes this gap.

Details
=======

This module adds a permission called "edit own user profile".
If you ensure the roles "anonymous user", "authenticated user" and "guest" all
do NOT have granted that permission, while all other roles do have it granted,
the Guest won't be able to change anything on her profile, while all other
authenticated users will still be able to do so.

The Guest will not see any tabs on her profile page (at "user/[UID]",
where [UID] is the guest user's actual user ID number) to edit it. If she
directly calls any path below her profile page "user/[UID]/X" (eg.
"user/[UID]/edit" or "user/[UID]/edit/password") she will be redirected back to
the profile view page.

Requirements
============

Drupal 6.x. No modules required.

Installation or Upgrade
=======================

This module follows Drupal's standard module installation procedure.

If Drupal is running on a Unix server, the most convenient way to install the
module is doing it directly on the sh shell (via SSH). The commands to enter as
the root user may be something like:

[root@server ~]# cd /etc/drupal6/all/modules
[root@server modules]# rm -rf guest guest-*
[root@server modules]# wget \
> http://ftp.drupal.org/files/projects/guest-6.x-1.2.tar.gz
[root@server modules]# chmod 600 guest-6.x-1.2.tar.gz
[root@server modules]# tar -zxf guest-6.x-1.2.tar.gz
[root@server modules]# chown -R apache:apache guest

After installation, load the "Administer » Site building » Modules"
(admin/build/modules) page and tick the "Guest" checkbox within the
"Access control" group.

Configuration
=============

After the module has been enabled, an info message appears on top of the
Modules admin page, containing a link to get help. Clicking that link will
guide you to everything you need to know and do.

The Help is also available at any time at "Administer » Help » Guest"
(admin/help/guest).

Uninstallation
==============

At the "Administer » Site building » Modules" (admin/build/modules) page
ensure the "Guest" checkbox within the "Access control" group is unticked.

Then entirely remove the "guest" directory from the modules directory:

[root@server modules]# rm -rf guest
